<%-- 
    Document   : index.jsp
    Created on : Sep 23, 2020, 3:09:43 PM
    Author     : vtexinfra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculator</title>
    </head>
    <body>
        <h1>Calculadora</h1>
        <form action="http://localhost:8080/calculator/CalculatorServlet" method="post">
            Valor 1: <input type="number" name="first_value" />
            Valor 2: <input type="number" name="second_value" />
            
            <br>
            
            
            Selecione uma operação
            <select name="operation">
                <option value="sum">
                    Soma
                </option>
                <option value="sub">
                    Subtração
                </option>
                <option value="mult">
                    Multiplicação
                </option>
                 <option value="div">
                    Divisão
                </option>
            </select>
            
            <br>
            
            <input type="submit" value="Calcular" />
        </form>
    </body>
</html>
