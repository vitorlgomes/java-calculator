/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.uff.ic.calculator;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;

/**
 *
 * @author vtexinfra
 */
public class CalculatorServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
        int first_value = Integer.parseInt(request.getParameter("first_value"));
        int second_value = Integer.parseInt(request.getParameter("second_value"));
        int counter = 1;
        int result = 0;
        
        String operation = request.getParameter("operation");
        
        if (operation.equals("sum")) {
            result = first_value + second_value;
        }
        
        if (operation.equals("sub")) {
            result = first_value - second_value;
        }
        
        if (operation.equals("mult")) {
            result = first_value * second_value;
        }
        
        if (operation.equals("div")) {
            result = first_value / second_value;
        }
        
        for (Cookie cookie : request.getCookies()) {
            if (cookie.getName().equals("counter")) {				
              counter = Integer.parseInt(cookie.getValue()) + 1;
            }
        }
        
        Cookie cookie = new Cookie("counter", String.valueOf(counter));
        
        response.addCookie(cookie);
  
        HttpSession session = request.getSession();
        session.setAttribute("counter", counter);
        session.setAttribute("result", result);
        session.setAttribute("operators", String.format("Primeiro operador - %s <br> Segundo operador - %s", first_value, second_value));

                
        request.getRequestDispatcher("/Result.jsp").forward(request, response);
      }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
   
}
