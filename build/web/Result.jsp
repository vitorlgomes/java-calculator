<%-- 
    Document   : Result.jsp
    Created on : Sep 23, 2020, 3:24:52 PM
    Author     : vtexinfra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Result</title>
    </head>
    <body>
        <h1>Resultado</h1>
       
            
        <p>O resultado é:  <%= session.getAttribute("result") %></p>
        <p><%= session.getAttribute("operators") %></p>
        <p>Acessos:  <%= session.getAttribute("counter") %></p>
        
    </body> 
</html>
